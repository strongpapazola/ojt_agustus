from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
import jwt
import datetime
from functools import wraps

app = Flask(__name__)

mydb = mysql.connector.connect(
    host="172.17.0.1",
    user="backend1",
    passwd="backend1",
    database="liveapp"
)        

@app.route("/barang", methods=['GET'])
def getbarang():
    try:
        cursor = mydb.cursor()
        cursor.execute("SELECT * FROM barang")
        result = []
        for i in cursor.fetchall():
            result.append({
                "id": i[0],
                "nama": i[1],
                "harga": i[2]
            })
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang/<int:id>", methods=['GET'])
def getbarangbyid(id):
    try:
        cursor = mydb.cursor()
        cursor.execute("SELECT * FROM barang WHERE id = %s" % (id,))
        data = cursor.fetchone()
        result = {
                    "id": data[0],
                    "nama": data[1],
                    "harga": data[2]
                }
        return jsonify({"data":result, "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    app.run(debug=True, host="0.0.0.0", port=1000)