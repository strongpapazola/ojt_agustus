from logging import debug
from flask import Flask, request, jsonify
import mysql.connector
import jwt
import datetime
from functools import wraps

app = Flask(__name__)

mydb = mysql.connector.connect(
    host="172.17.0.1",
    user="backend1",
    passwd="backend1",
    database="liveapp"
)        

@app.route("/barang", methods=['POST'])
def insertbarang():
    try:
        data = request.json
        cursor = mydb.cursor()
        sql = "INSERT INTO barang (id, nama, harga) VALUES (NULL, %s, %s)"
        value = (data['nama'], data['harga'])
        cursor.execute(sql, value)
        mydb.commit()
        return jsonify({"data":"1 Record Inserted!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang/<int:id>", methods=['PUT'])
def updatebarangbyid(id):
    try:
        data = request.json
        cursor = mydb.cursor()
        sql = "UPDATE barang SET nama = %s, harga = %s WHERE id = %s"
        value = (data['nama'], data['harga'], id)
        cursor.execute(sql, value)
        mydb.commit()
        return jsonify({"data":"1 Record Affected!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

@app.route("/barang/<int:id>", methods=['DELETE'])
def deletebarangbyid(id):
    try:
        cursor = mydb.cursor()
        cursor.execute("DELETE FROM barang WHERE id = %s" % (id,))
        return jsonify({"data":"1 Record Deleted!", "code": 200}), 200
    except Exception as e:
        return jsonify({"data":str(e), "code": 500}), 500

if "__main__" == __name__:
    app.run(debug=True, host="0.0.0.0", port=1000)